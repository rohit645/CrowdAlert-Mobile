# CrowdAlert-Mobile app

Crowdalert is a hybrid mobile application which helps it users for reporting and viewing incidents around the globe. It uses markers to plot the location of incident on the google maps and also servers the function of notifying the users, whenever any incident is reported in their local area.The app is built on React Native platform, so that it can provide common functionalities for both Android and iOS.

### Link to project’s repository

https://gitlab.com/aossie/CrowdAlert-Mobile

### Link to mobile app

https://drive.google.com/file/d/117GC8prMgI2xoUMuFA8jN5WzLGcqjHez/view?usp=drivesdk

## Goals

#### Achieved

*   Added initial tips for first time users which helps in giving a general introduction to all features present in the app.
*   Added email verification of the user which helps in making sure that incidents are added only by verfied users and no fake incidents are created.
*   Added SOS call feature, which helps in fetching the contact through google maps and calling the place from within the app. 
*   Added new firestation screen which shows all the firestations present nearby the users location.
*  Added loading-spinner-overlay on various screens, thus giving good UX to the app.
*   Added jest tests for all the actions present in the app code.
*   Added jest tests for all the Reducers present in the app code
*   Added jest tests for all the components present in the app code.
*   Reduced APK size for different CPU architectures significantly.
*   Added android production stage in the pipeline
*   Fixed google searchbar ui, which used to distort whenever searching for places.
*   Removed unnecessary rendering of components again and again and removed unused line of codes.
*   Improved fetching of incidents created by the user
*   Improved styling on every screen in iOS and also fixed bugs in it.
*  Added iOS setup guide, and fixed dependency errors.
*  Made UI/UX improvements on various screens.
*  Improved initial intro slides in the app.
*  Improved headers of various screens.


#### Pending

*   Adding auto deployment stage in the pipeline.(this will be added, once APK is published)

## Contributions

*  Added initial tips for first time users using package [react-native-tips](https://www.npmjs.com/package/react-native-tips) which helped in giving various features such as waterfall-tips and thus showing one tip after the another.
*  Sending email verification to the user with the help of function [sendEmailVerification](https://firebase.googleblog.com/2017/02/email-verification-in-firebase-auth.html) provided by firebase.
*  Added SOS-call feature with the help of  package [react-native-open-maps](https://www.npmjs.com/package/react-native-open-maps ) which helps in opening google maps and thus fetching the contact details from thereon.
* Added new firestations screen along with hospitals and policestations.
* Added loading-spinner-overlays with the help of package [react-native-loadin-spinner-overlay](https://www.npmjs.com/package/react-native-loading-spinner-overlay).
* Added jest tests for all components with help of packages such as [jest](https://jestjs.io/docs/en/getting-started),[enzyme](https://airbnb.io/enzyme/docs/api/),enzyme-adapter-react-16,babel-jest,identity-obj-proxy etc.
* Improved linear gradient of background color on various screens with the help of package [react-native-linear-gradient](https://www.npmjs.com/package/react-native-linear-gradient).
* Improved iOS styling on every screen, also fixed bugs related with iOS dependencies and added iOS setup guide.

## Other Contributions

### Bug Fixes

*  Improved dashboard scroll of incidents.
*  Searchbar overlay icons issue fixed.
*  APK build was fixed which was failing because of dependency updates.
*  Issue of app not closing on backpress from login screen was fixed.
*  Email settings changed to text, otherwise users were able to change their emails which was resulting in creation of new users in the database.
*  Modified pipeline installation commands, which fixes the debug apk made in the build stage of the pipeline.
*  ScrollViews added in landscape mode of the app.
*  Offline notice added, which notifies users when they are using the app but not connected to the internet.
*  Screen glitch was fixed which was causing the screens to have some margins below the status bar.
*  Profile update issue resolved, which avoids showing register message and only shows profile update message when profile is updated.


## Future Scope of the project

*   Features such as spam detection can be added in the future.
*   The CrowdAlert-Web and Mobile should be more closely integrated in the future.

## Merge Requests

*   [ profile update issue resolved ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/67)
*   [Screen glitch resolved ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/73)
*   [Offline Notice added](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/76)
*   [ScrollViews Added ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/81)
*   [modified pipeline intallation commands ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/82)
*   [Email setting changed to text ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/83)
*   [ initial tips for first time users ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/84)
*   [email verification added ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/85)
*   [App closes from login screen on BackPress ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/86)
*   [sos-call feature added, fire station screen created ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/87)
*   [apk build failure due to dependency updates resolved ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/88)
*   [Loading-spinner-overlay put on various screens with messages ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/89)
*   [Testing Actions ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/90)
*   [Testing Reducers ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/91)
*   [Testing components ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/92)
*   [Fixing pipeline APK  ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/93)
*   [APK size reduced ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/94)
*   [Added android production stage ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/95)
*   [Searchbar UI fixed  ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/96)
*   [Unnecessary codes removed  ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/97)
*   [UI improvements   ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/98)
*   [Searchbar overlay icons issue fixed   ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/99)
*   [Increasing fetching of user incidents   ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/100)
*   [ Modified URL of fetching nearby emergencyPlaces   ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/101)
*   [Improved dashboard scroll of incidents   ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/102)
*   [Improved styling on every screen in iOS  ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/103)
*   [Added ios-setup guide   ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/104)
*   [Improved intro slides   ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/105)
*   [ Improved headers of various screens ](https://gitlab.com/aossie/CrowdAlert-Mobile/merge_requests/106)
